package com.zhaijiong.hedis.cp;

import com.google.common.collect.Maps;
import com.zhaijiong.hedis.Constants;
import com.zhaijiong.hedis.zk.ZooKeeperUtil;
import org.apache.hadoop.hbase.CoprocessorEnvironment;
import org.apache.hadoop.hbase.HConstants;
import org.apache.hadoop.hbase.KeyValue;
import org.apache.hadoop.hbase.client.Delete;
import org.apache.hadoop.hbase.client.Get;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.coprocessor.BaseRegionObserver;
import org.apache.hadoop.hbase.coprocessor.ObserverContext;
import org.apache.hadoop.hbase.coprocessor.RegionCoprocessorEnvironment;
import org.apache.hadoop.hbase.regionserver.wal.WALEdit;
import org.apache.hadoop.hbase.util.Bytes;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import redis.clients.jedis.JedisShardInfo;
import redis.clients.jedis.ShardedJedis;

import java.io.IOException;
import java.util.List;
import java.util.Map;

public class DeleteRedisPrePutCoprocessor extends BaseRegionObserver {
    private static final Logger LOG = LoggerFactory.getLogger(DeleteRedisPrePutCoprocessor.class);
    private static final String COPROCESSOR_NAME = DeleteRedisPrePutCoprocessor.class.getSimpleName();
    private ZooKeeperUtil zk;
    private ShardedJedis jedis;

    @Override
    public void start(CoprocessorEnvironment e) throws IOException {
        LOG.info(COPROCESSOR_NAME + " is starting");
        zk = new ZooKeeperUtil(e.getConfiguration().get(HConstants.ZOOKEEPER_QUORUM),"hedis");
        List<JedisShardInfo> shards = zk.getShards();
        jedis = new ShardedJedis(shards);
    }

    @Override
    public void stop(CoprocessorEnvironment e) throws IOException {
        LOG.info(COPROCESSOR_NAME + " is stopping");
        jedis.disconnect();
        zk.close();
    }

    @Override
    public void prePut(ObserverContext<RegionCoprocessorEnvironment> e, Put put, WALEdit edit, boolean writeToWAL) throws IOException {
        jedis.del(put.getRow());
    }

    @Override
    public void postGet(ObserverContext<RegionCoprocessorEnvironment> e, Get get, List<KeyValue> results) throws IOException {
        Map<byte[], byte[]> kvs = Maps.newHashMap();
        if(results.size()==1&& Constants.REDIS_TYPE_STR.equals(Bytes.toString(results.get(0).getQualifier()))){
            jedis.set(results.get(0).getRow(),results.get(0).getValue());
        }else if(results.size()>1){
            for(KeyValue kv:results){
                kvs.put(kv.getQualifier(),kv.getValue());
            }
            jedis.hmset(get.getRow(),kvs);
        }
    }

    @Override
    public void preDelete(ObserverContext<RegionCoprocessorEnvironment> e, Delete delete, WALEdit edit, boolean writeToWAL) throws IOException {
        jedis.del(delete.getRow());
    }
}
