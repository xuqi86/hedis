package zk;

import com.zhaijiong.hedis.zk.ZooKeeperUtil;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import redis.clients.jedis.JedisShardInfo;
import redis.clients.jedis.ShardedJedis;

import java.util.List;
import java.util.Set;

/**
 * ZooKeeperUtil Tester.
 *
 * @author <Authors name>
 * @version 1.0
 * @since <pre>一月 10, 2014</pre>
 */
public class ZooKeeperUtilTest {
    private ZooKeeperUtil util;

    @Before
    public void before() throws Exception {
        util = new ZooKeeperUtil("m:2181","hedis");
    }

    @After
    public void after() throws Exception {
        util.close();
    }

    public void prepare() throws Exception {
        util.getCurator().create().forPath("172.16.121.226:6379","".getBytes());
    }


    public void testGetList() throws Exception {
        Set<String> list = util.getList();
        for(String address :list){
            System.out.println(address);
        }
    }

    @Test
    public void testGetShards() throws Exception {
        List<JedisShardInfo> shards = util.getShards();
        ShardedJedis jedis = new ShardedJedis(shards);
        System.out.println(jedis.get("eryk"));
    }

} 
