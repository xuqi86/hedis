package com.zhaijiong.hedis;

import com.google.common.base.Stopwatch;
import com.zhaijiong.hedis.Hedis;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * Hedis Tester.
 *
 * @author <Authors name>
 * @version 1.0
 * @since <pre>一月 9, 2014</pre>
 */
public class HedisTest {
    private Hedis hedis;
    private Stopwatch watcher;

    @Before
    public void before() throws Exception {
        hedis = new Hedis("config.xml");
        hedis.init();
    }

    @After
    public void after() throws Exception {
        hedis.close();
    }
    @Test
    public void testhSet() throws Exception {
        watcher = Stopwatch.createStarted();
        Long retVal = hedis.hset("eryk","qipan","1886123");
        print(String.valueOf(retVal));
    }
    @Test
    public void testhGet() throws Exception {
        watcher = Stopwatch.createStarted();
        String retVal = hedis.hget("eryk","xuqi");
        print(retVal);
    }

    @Test
    public void testSet() throws Exception {
        watcher = Stopwatch.createStarted();
        String retVal = hedis.set("eryk", "1886123");
        print(retVal);
    }

    @Test
    public void testGet() throws Exception {
        watcher = Stopwatch.createStarted();
        String retVal = hedis.get("haha");
        print(retVal);
    }

    @Test
    public void testExist() throws Exception {
        watcher = Stopwatch.createStarted();
        boolean retVal = hedis.exists("haha");
        print(String.valueOf(retVal));
    }

    @Test
    public void testHGetAll() throws Exception{
        watcher = Stopwatch.createStarted();
        Map<String, String> retVal = hedis.hgetAll("eryk");
        print(String.valueOf(retVal));
        for(Map.Entry<String,String> kv : retVal.entrySet()){
            System.out.println(kv.getKey()+":"+kv.getValue());
        }
    }

    @Test
    public void testMGet() throws Exception {
        watcher = Stopwatch.createStarted();
        List<String> mget = hedis.mget("eryk", "123123", "hello", "world");
        print("ok");
        for(String value:mget){
            System.out.println(value);
        }
    }

    @Test
    public void testMSet() throws Exception {
        watcher = Stopwatch.createStarted();
        hedis.mset("eryk","123123","hello","world");
        print("ok");
    }

    public void print(String retVal){
        System.out.println(String.format("timecost: %sms",watcher.elapsed(TimeUnit.MILLISECONDS)));
        System.out.println("return:"+retVal);
    }
} 
