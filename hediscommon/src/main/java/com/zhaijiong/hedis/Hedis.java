package com.zhaijiong.hedis;

import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.util.Bytes;
import org.hbase.async.KeyValue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import redis.clients.jedis.Jedis;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class Hedis {
    private static Logger LOG = LoggerFactory.getLogger(Hedis.class);

    private HBaseConnection hbaseConn;
    private Configuration config;

    public Hedis(String configfile) {
        config = new Configuration();
        config.addResource(configfile);
    }

    public void init() {
        JedisFactory.REDIS_HOST = config.get(Constants.REDIS_HOST);
        JedisFactory.REDIS_PORT = config.getInt(Constants.REDIS_PORT, 6379);
        JedisFactory.createAndConnectPool();

        hbaseConn = new HBaseConnection();
        try {
            hbaseConn.connect(config.get(Constants.ZK_QUORUM), config.get(Constants.ZK_HBASE_ZNODE));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void close(){
        hbaseConn.close();
    }

    public String set(final String key, final String value) {
        hbaseConn.putData(key,Constants.REDIS_TYPE_STR, value);
        String retVal = JedisFactory.withJedisDo(new JedisFactory.JWork<String>() {
            @Override
            public String work(Jedis p) {
                return p.set(key, value);
            }
        });
        return retVal;
    }

    public String get(final String key) {
        String retVal = JedisFactory.withJedisDo(new JedisFactory.JWork<String>() {
            @Override
            public String work(Jedis p) {
                return p.get(key);
            }
        });
        if (!Strings.isNullOrEmpty(retVal)) {
            return retVal;
        }else{
            try {
                retVal = Bytes.toString(hbaseConn.getData(Bytes.toBytes(key)).get(0).value());
                return retVal;
            } catch (Exception e) {
                e.printStackTrace();
                return "";
            }
        }
    }

    public Boolean exists(final String key) {
        boolean flag = false;
        flag=JedisFactory.withJedisDo(new JedisFactory.JWork<Boolean>() {
            @Override
            public Boolean work(Jedis p) {
                return p.exists(key);
            }
        });
        if(flag){
            return flag;
        }
        try {
            flag = !Strings.isNullOrEmpty(Bytes.toString(hbaseConn.getData(Bytes.toBytes(key)).get(0).value()));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return flag;
    }

    public void mset(final String...kvs){
        for(int i = 0;i<kvs.length-2;i++){
            hbaseConn.putData(kvs[i],Constants.REDIS_TYPE_STR,kvs[i+1]);
        }
    }

    public List<String> mget(final String...keys) throws Exception {
        Set<String> values = Sets.newHashSet();
        values.addAll(JedisFactory.withJedisDo(new JedisFactory.JWork<List<String>>() {
            @Override
            public List<String> work(Jedis p) {
                return p.mget(keys);
            }
        }));
        values.remove(null);
        if(values.size()==0){
            for(String key:keys){
                ArrayList<KeyValue> kvs = hbaseConn.getData(Bytes.toBytes(key));
                for(KeyValue kv : kvs){
                    if(Constants.REDIS_TYPE_STR.equals(Bytes.toString(kv.qualifier()))){
                        values.add(Bytes.toString(kv.value()));
                    }
                }
            }
        }
        return Lists.newArrayList(values);
    }

    public Long hset(final String key,final String field,final String value){
        hbaseConn.putData(key,field,value);
        Long retVal = JedisFactory.withJedisDo(new JedisFactory.JWork<Long>() {
            @Override
            public Long work(Jedis p) {
                return p.hset(key, field, value);
            }
        });
        return retVal;
    }

    public String hget(final String key,final String field){
        String retVal = JedisFactory.withJedisDo(new JedisFactory.JWork<String>() {
            @Override
            public String work(Jedis p) {
                return p.hget(key, field);
            }
        });
        if (!Strings.isNullOrEmpty(retVal)){
            return retVal;
        }else {
            try {
                List<KeyValue> kvs = hbaseConn.getData(Bytes.toBytes(key));
                for(KeyValue kv:kvs){
                    if(Bytes.compareTo(kv.qualifier(),field.getBytes())==0){
                        retVal = Bytes.toString(kv.value());
                        break;
                    }
                }
                return retVal;
            } catch (Exception e) {
                e.printStackTrace();
                return Constants.REDIS_RETURN_ERROR;
            }
        }
    }

    public Map<String,String> hgetAll(final String key){
        final Map<String, String> keys = Maps.newHashMap();
        keys.putAll(JedisFactory.withJedisDo(new JedisFactory.JWork<Map<String, String>>() {
            @Override
            public Map<String, String> work(Jedis p) {
                return p.hgetAll(key);
            }
        }));
        if(keys.size()==0){
            try {
                ArrayList<KeyValue> kvs = hbaseConn.getData(Bytes.toBytes(key));
                for(KeyValue kv : kvs){
                    if(!Constants.REDIS_TYPE_STR.equals(Bytes.toString(kv.qualifier()).trim())){
                        keys.put(Bytes.toString(kv.qualifier()),Bytes.toString(kv.value()));
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return keys;
    }

    public Set<String> hkeys(final String key) throws Exception {
        final Set<String> keys = Sets.newHashSet();
        keys.addAll(JedisFactory.withJedisDo(new JedisFactory.JWork<Set<String>>() {
            @Override
            public Set<String> work(Jedis p) {
                return p.hkeys(key);
            }
        }));
        if(keys.size()==0){
            ArrayList<KeyValue> kvs = hbaseConn.getData(Bytes.toBytes(key));
            for(KeyValue kv : kvs){
                if(!Constants.REDIS_TYPE_STR.equals(Bytes.toString(kv.qualifier()).trim())){
                    keys.add(Bytes.toString(kv.qualifier()));
                }
            }
        }
        return keys;
    }

    public Long hlen(final String key) throws Exception {
        Long length = JedisFactory.withJedisDo(new JedisFactory.JWork<Long>() {
            @Override
            public Long work(Jedis p) {
                return p.hlen(key);
            }
        });
        if(length==0){
            length = Long.valueOf(hbaseConn.getData(Bytes.toBytes(key)).size());
        }
        return length;
    }

    public List<String> hvals(final String key) throws Exception {
        List<String> values = Lists.newArrayList();
        values.addAll(JedisFactory.withJedisDo(new JedisFactory.JWork<List<String>>() {
            @Override
            public List<String> work(Jedis p) {
                return p.hvals(key);
            }
        }));
        if(values.size()==0){
            ArrayList<KeyValue> kvs = hbaseConn.getData(Bytes.toBytes(key));
            for(KeyValue kv : kvs){
                values.add(Bytes.toString(kv.value()));
            }
        }
        return values;
    }

    public boolean hexists(final String key,final String field) throws Exception {
        Boolean checkExist = JedisFactory.withJedisDo(new JedisFactory.JWork<Boolean>() {
            @Override
            public Boolean work(Jedis p) {
                return p.hexists(key, field);
            }
        });
        if(!checkExist){
            ArrayList<KeyValue> kvs = hbaseConn.getData(Bytes.toBytes(key));
            for(KeyValue kv :kvs){
                if(field.equals(Bytes.toString(kv.qualifier()))){
                    return true;
                }
            }
        }
        return checkExist;
    }
}
