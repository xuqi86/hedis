package com.zhaijiong.hedis;

import org.apache.hadoop.hbase.HConstants;

public class Constants {

    public final static String ZK_QUORUM = HConstants.ZOOKEEPER_QUORUM;
    public final static String ZK_HBASE_ZNODE = HConstants.ZOOKEEPER_ZNODE_PARENT;

    public final static String REDIS_HOST = "redis.address";
    public final static String REDIS_PORT = "redis.port";

    public final static String REDIS_TYPE_STR = "k";
    public final static String REDIS_TYPE_LIST = "l";
    public final static String REDIS_TYPE_SET = "s";
    public final static String REDIS_TYPE_SORTEDSET = "z";
    public final static String REDIS_TYPE_HASH = "h";

    public final static String REDIS_RETURN_ERROR = "nil";
}
