package com.zhaijiong.hedis;

import com.google.common.base.Charsets;
import com.stumbleupon.async.Callback;
import org.apache.hadoop.hbase.util.Bytes;
import org.hbase.async.GetRequest;
import org.hbase.async.HBaseClient;
import org.hbase.async.KeyValue;
import org.hbase.async.PutRequest;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Created with IntelliJ IDEA.
 * User: eryk
 * Date: 14-1-9
 * Time: 上午11:47
 * To change this template use File | Settings | File Templates.
 */
public class HBaseConnection {
    private final String TABLE_NAME;

    private final String CF;

    private HBaseClient hbaseClient;

    public HBaseConnection(String tableName,String columnfamily){
        this.TABLE_NAME = tableName;
        this.CF = columnfamily;
    }

    public HBaseConnection(){
        this.TABLE_NAME = "hedis";
        this.CF = "v";
    }

    public void connect(String quorum,String znode) throws Exception {
        hbaseClient = new HBaseClient(quorum, znode, Executors.newCachedThreadPool());

        final CountDownLatch latch = new CountDownLatch(1);
        final AtomicBoolean fail = new AtomicBoolean(false);
        hbaseClient.ensureTableFamilyExists(TABLE_NAME, CF).addCallbacks(
                new Callback<Object, Object>() {
                    @Override
                    public Object call(Object arg) throws Exception {
                        latch.countDown();
                        return null;
                    }
                },
                new Callback<Object, Object>() {
                    @Override
                    public Object call(Object arg) throws Exception {
                        fail.set(true);
                        latch.countDown();
                        return null;
                    }
                }
        );
        try {
            latch.await();
        } catch (InterruptedException e) {
            throw new Exception("Interrupted", e);
        }
        if(fail.get()) {
            throw new Exception("Table or Column Family doesn't exist");
        }
    }

    public void putData(String rowKey,String column, String data){
        PutRequest putRequest = new PutRequest(TABLE_NAME.getBytes(Charsets.UTF_8), Bytes.toBytes(rowKey),
                CF.getBytes(Charsets.UTF_8), column.getBytes(Charsets.UTF_8),
                data.getBytes(Charsets.UTF_8));
        hbaseClient.put(putRequest);
    }

    public ArrayList<KeyValue> getData(byte[] rowKey) throws Exception {
        GetRequest getRequest = new GetRequest(TABLE_NAME, rowKey);
        ArrayList<KeyValue> kvs = hbaseClient.get(getRequest).join();
        return kvs;
    }

    public void close(){
        hbaseClient.shutdown();
    }
}
