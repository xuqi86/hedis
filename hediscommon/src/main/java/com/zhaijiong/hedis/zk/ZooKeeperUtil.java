package com.zhaijiong.hedis.zk;

import com.google.common.base.Joiner;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.CuratorFrameworkFactory;
import org.apache.curator.retry.ExponentialBackoffRetry;
import redis.clients.jedis.JedisShardInfo;

import java.util.List;
import java.util.Set;

/**
 * Created with IntelliJ IDEA.
 * User: eryk
 * Date: 14-1-10
 * Time: 下午7:39
 * To change this template use File | Settings | File Templates.
 */
public class ZooKeeperUtil {
    private CuratorFramework curator;

    public ZooKeeperUtil(String quorum, String namespace){
        CuratorFrameworkFactory.Builder buidler = CuratorFrameworkFactory.builder()
                .retryPolicy(new ExponentialBackoffRetry(5000, 5))
                .connectString(quorum)
                .namespace(namespace);
        curator = buidler.build();
        curator.start();
    }

    public CuratorFramework getCurator(){
        return curator;
    }

    public Set<String> getList(){
        Set<String> redisServers = Sets.newHashSet();
        try {
            redisServers.addAll(curator.getChildren().forPath(""));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return redisServers;
    }

    public List<JedisShardInfo> getShards(){
        List<JedisShardInfo> shards = Lists.newArrayList();
        Set<String> children = getList();
        for(String jedis:children){
            if(jedis.contains(":")){
                String[] address = jedis.split(":");
                JedisShardInfo si = new JedisShardInfo(address[0], Integer.parseInt(address[1]));
                shards.add(si);
            }
        }
        return shards;
    }

    public void createNode(String ip,int port) throws Exception {
        String path = Joiner.on(":").join(ip,port);
        if(curator.checkExists().forPath(path)==null){
            curator.create().forPath(path,"".getBytes());
        }
    }

    public void close(){
        curator.close();
    }

    public static void main(String[] args) {

    }
}
