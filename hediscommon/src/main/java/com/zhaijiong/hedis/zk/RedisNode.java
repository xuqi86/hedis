package com.zhaijiong.hedis.zk;

import com.google.common.base.Joiner;

public class RedisNode{
    private final String ip;
    private final int port;

    public RedisNode(String ip,int port){
        this.ip = ip;
        this.port = port;
    }

    public String getIp() {
        return ip;
    }

    public int getPort() {
        return port;
    }

    public String getZKPath(){
        return Joiner.on(":").join(ip,port);
    }

    @Override
    public String toString() {
        return "RedisNode{" +
                "ip='" + ip + '\'' +
                ", port=" + port +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        RedisNode redisNode = (RedisNode) o;

        if (port != redisNode.port) return false;
        if (ip != null ? !ip.equals(redisNode.ip) : redisNode.ip != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = ip != null ? ip.hashCode() : 0;
        result = 31 * result + port;
        return result;
    }
}
