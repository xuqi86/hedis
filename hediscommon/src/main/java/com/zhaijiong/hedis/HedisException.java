package com.zhaijiong.hedis;

/**
 * Created with IntelliJ IDEA.
 * User: eryk
 * Date: 14-1-9
 * Time: 下午1:16
 * To change this template use File | Settings | File Templates.
 */
public class HedisException extends Exception {
    public HedisException() {
        super();
    }

    public HedisException(String message) {
        super(message);
    }

    public HedisException(String message, Throwable cause) {
        super(message, cause);
    }

    public HedisException(Throwable cause) {
        super(cause);
    }
}
