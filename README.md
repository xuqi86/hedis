Hedis=HBase+Redis
=====
Hedis结合了HBase存储海量数据能力和Redis支持高并发访问的优势，适用于应用中存在热点数据和长尾数据读写问题的场景。它依赖zookeeper存储Redis地址信息，使用HBase coprocesser清理Redis失效数据，并缓存数据。Hedis提供一套api接口来访问数据层。

## 目前Hedis支持的Redis数据结构：
### 1. String
* exists
* get
* set
* mset

### 2. Hash
* hget
* hgetAll
* hkeys
* hset
* hkeys
* hvals
* hlen
* hexists

1. Apache HBase is an open-source, distributed, versioned, column-oriented store modeled after Google' Bigtable: A Distributed Storage System for Structured Data by Chang et al.
2. Redis is an in-memory database that persists on disk. The data model is key-value, but many different kind of values are supported: Strings, Lists, Sets, Sorted Sets, Hashes